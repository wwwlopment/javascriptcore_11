function extractCurrencyValue (str) {
  return parseInt(str.replace(/\D+/g,""));
}

console.log(extractCurrencyValue("$ 120"));
